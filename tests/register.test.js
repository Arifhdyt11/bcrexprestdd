const request = require("supertest");
const app = require("../app");

const user = {
  name: "Arif",
  email: "arif@gmail.com",
  password: "123456",
};

describe("Test endpoint register", () => {
  it("should return 422 if the email is already registered", async () => {
    try {
      await request(app).post("/api/v1/users").send(user);

      const res = await request(app).post("/api/v1/users");

      expect(res.statusCode).toBe(201);
      expect(res.body).toHaveProperty("status", "Success");
      expect(res.body).toHaveProperty(
        "message",
        "History created successfully"
      );
    } catch (err) {
      console.log(err.message);
    }
  });

  it("should return 201 if the user is successfully registered", async () => {
    const name = "Hello";
    const email = `member${Math.random().toString().substring(12)}@gmail.com`;
    const password = "123";
    try {
      await request(app)
        .post("/api/v1/users")
        .set("Content-Type", "application/json")
        .send({ name, email, password });

      const res = await request(app).post("/api/v1/users");

      expect(res.statusCode).toBe(201);
      expect(res.body.accessToken).toBeDefined();
    } catch (err) {
      console.log(err.message);
    }
  });
});
